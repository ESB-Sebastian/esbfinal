package com.logixboardesb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * LogixBoardESBApplication
 *
 * @author Hector Osorio
 * @since 08-Apr-2019
 */
@EnableAutoConfiguration
@EnableScheduling
public class LogixBoardESBApplication extends SpringBootServletInitializer
{
    public static void main(String[] args)
    {
        SpringApplication.run(LogixBoardESBApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.sources(LogixBoardESBApplication.class);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder()
    {
        return new BCryptPasswordEncoder();
    }
}
