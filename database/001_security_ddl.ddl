DROP TABLE IF EXISTS `sc_notification`;
DROP TABLE IF EXISTS `sc_tenantintegration`;
DROP TABLE IF EXISTS `sc_customeruser`;
DROP TABLE IF EXISTS `sc_customer`;
DROP TABLE IF EXISTS `sc_tenantsetting`;
DROP TABLE IF EXISTS `sc_tenantuser`;
DROP TABLE IF EXISTS `sc_tenant`;
DROP TABLE IF EXISTS `sc_user`;

CREATE TABLE `sc_user` 
(
  `id`        int(11) NOT NULL AUTO_INCREMENT,
  `name`      varchar(255) NOT NULL,
  `lastname`  varchar(255),
  `email`     varchar(255),
  `login`     varchar(100) NOT NULL,
  `tenantschema`    varchar(100) NOT NULL,
  `password`  LONGTEXT NOT NULL,
  `role` 	  varchar(100) NOT NULL,
  `city` 	  varchar(255) NULL,
  `operationemail` VARCHAR(255),
  `enabled`   varchar(5) NOT NULL,
  `last_authentication_date` DATETIME,
  `creationdate` DATETIME,
  `lastupdate` DATETIME,
  `createdby` VARCHAR(255),
  `updatedby` VARCHAR(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `sc_user` ADD UNIQUE INDEX `login_UNIQUE` (`login`,`tenantschema` ASC);

CREATE TABLE `sc_tenant` 
(
  `id`        int NOT NULL AUTO_INCREMENT,
  `name`      varchar(100) NOT NULL,
  `tenantschema`    varchar(100),
  `creationdate` DATETIME,
  `lastupdate` DATETIME,
  `createdby` VARCHAR(255),
  `updatedby` VARCHAR(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `sc_tenant` ADD UNIQUE INDEX `sc_tenant_name_unique` (`name` ASC);
ALTER TABLE `sc_tenant` ADD UNIQUE INDEX `sc_tenant_tenantschema_unique` (`tenantschema` ASC);

CREATE TABLE `sc_tenantuser` 
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `tenantid` INT NOT NULL,
  `userid` INT NOT NULL,
  `adminuser` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sc_tenantuser_sc_tenant1` FOREIGN KEY (`tenantid`) REFERENCES `sc_tenant` (`id`),
  CONSTRAINT `fk_sc_tenantuser_sc_user1` FOREIGN KEY (`userid`) REFERENCES `sc_user` (`id`)
)ENGINE = InnoDB;

CREATE TABLE `sc_tenantsetting`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `tenantid` INT NOT NULL,
  `type` varchar(50),
  `value` LONGTEXT,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sc_tenantsetting_sc_tenant` FOREIGN KEY (`tenantid`) REFERENCES `sc_tenant` (`id`)
)ENGINE = InnoDB;

CREATE TABLE `sc_customer` 
(
  `id`        int NOT NULL AUTO_INCREMENT,
  `name`      varchar(100) NOT NULL,  
  `tenantid`  int NOT NULL,
  `companyname` VARCHAR(100),
  `industry` VARCHAR(100),
  `address` VARCHAR(255),
  `locationtype` VARCHAR(100),
  `helpunloading` VARCHAR(10),
  `appontmentneeded` VARCHAR(10),
  `hoursoperation` VARCHAR (1000),
  `specialdeliveryinstructions` VARCHAR(500),
  `contactname` VARCHAR(100),
  `contactphone` VARCHAR(100),
  `fax` VARCHAR(100),
  `agentname` VARCHAR(100),
  `identificationcode` VARCHAR(100),
  `salesimport` VARCHAR(255),
  `salesexport` VARCHAR(255),
  `operations` VARCHAR(255),
  `keyaccount` VARCHAR(255),
  `billing` VARCHAR(255),
  `creationdate` DATETIME,
  `lastupdate` DATETIME,
  `createdby` VARCHAR(255),
  `updatedby` VARCHAR(255),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sc_customer_sc_tenant1` FOREIGN KEY (`tenantid`) REFERENCES `sc_tenant` (`id`)
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `sc_customer` ADD UNIQUE INDEX `sc_customer_name_unique` (`name`, `tenantid` ASC);

CREATE TABLE `sc_customersetting`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `customerid` INT NOT NULL,
  `type` varchar(50),
  `value` LONGTEXT,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sc_customersetting_sc_customer` FOREIGN KEY (`customerid`) REFERENCES `sc_customer` (`id`)
);

CREATE TABLE `sc_customeruser` 
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `customerid` INT NOT NULL,
  `userid` INT NOT NULL,
  `adminuser` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sc_customeruser_sc_customer1` FOREIGN KEY (`customerid`) REFERENCES `sc_customer` (`id`),
  CONSTRAINT `fk_sc_customeruser_sc_user1` FOREIGN KEY (`userid`) REFERENCES `sc_user` (`id`)
) ENGINE = InnoDB;

CREATE TABLE `sc_tenantintegration` 
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `tenantid` INT NOT NULL,
  `process` VARCHAR(100),
  `adapter` VARCHAR(50),
  `url` VARCHAR(250),
  `parameters` LONGTEXT,
  `headers` LONGTEXT,
  `enable` VARCHAR(5),
  `period` INT,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_sc_tenantintegration_sc_tenant` FOREIGN KEY (`tenantid`) REFERENCES `sc_tenant` (`Id`));

CREATE TABLE `sc_notification`
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(45) NOT NULL,
  `message` varchar(250) NOT NULL,
  `creationdate` datetime NOT NULL,
  `destinationuserid` int(11) NOT NULL,
  `isread` varchar(1) NOT NULL,
  `reference` varchar(250) NOT NULL,
  `elementid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sc_notification_sc_user` (`destinationuserid`),
  CONSTRAINT `fk_sc_notification_sc_user` FOREIGN KEY (`destinationuserid`) REFERENCES `sc_user` (`id`)
);

DROP TABLE IF EXISTS `sc_customerdashboard`;
CREATE TABLE `sc_customerdashboard`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `customerid` INT,
  `type` varchar(50),
  `sqlquery` LONGTEXT,
  `response` LONGTEXT,
  PRIMARY KEY (`id`)
);
