--Basic Data
insert into sc_user(id, name,lastname, email, login, tenantschema, password, role, enabled, last_authentication_date, creationdate, createdby, lastupdate, updatedby)
values (1, "Admin User","admin", "administrator@manager.com", "admin", "logixboard", "$2a$10$pgCVwChmDYm6YoUtljr.rOR.43HywGfOXupkqVEeBZBRi0SYKks/W", "Administrator", "Y", sysdate(), sysdate(), 'system', now(),'system');

-- Tenant Integration
INSERT INTO `logixboard`.`sc_tenantintegration` (`id`, `tenantid`, `process`, `adapter`, `url`,`headers`,`parameters`, `enable`)
VALUES (1, 1, 'REQUEST_QUOTE', 'SimulatorAdapter', 'http://127.0.0.1:8091/logixboard/api/quotes',
'{"Content-Type": "application/json", "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiJ9.4iavxQlrLWhEdHrD_wxVtUMHs60bt0gXFyJUzF87jU6e52kc3bv3NrTKwJlPnxrXJPPx9D6qOdupW5OS9sZc8g"}', NULL, 'N');

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`, `tenantid`, `process`, `adapter`, `url`,`headers`,`parameters`, `enable`)
VALUES (2, 1, 'REQUEST_BOOKING', 'SimulatorAdapter', 'http://127.0.0.1:8091/logixboard/api/bookings',
'{"Content-Type": "application/json", "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiJ9.4iavxQlrLWhEdHrD_wxVtUMHs60bt0gXFyJUzF87jU6e52kc3bv3NrTKwJlPnxrXJPPx9D6qOdupW5OS9sZc8g"}', NULL, 'N');

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`, `tenantid`, `process`, `adapter`, `url`,`headers`,`parameters`, `enable`)
VALUES (3, 1, 'GETTING_QUOTE', 'SimulatorAdapter', 'http://127.0.0.1:8091/logixboard/api/quotes',
'{"Content-Type": "application/json", "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiJ9.4iavxQlrLWhEdHrD_wxVtUMHs60bt0gXFyJUzF87jU6e52kc3bv3NrTKwJlPnxrXJPPx9D6qOdupW5OS9sZc8g"}', NULL, 'N');

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`, `tenantid`, `process`, `adapter`, `url`,`headers`,`parameters`, `enable`)
VALUES (4, 1, 'GETTING_SHIPMENT', 'SimulatorAdapter', 'http://127.0.0.1:8091/logixboard/api/shipments',
'{"Content-Type": "application/json", "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiJ9.4iavxQlrLWhEdHrD_wxVtUMHs60bt0gXFyJUzF87jU6e52kc3bv3NrTKwJlPnxrXJPPx9D6qOdupW5OS9sZc8g"}', NULL, 'N');

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`, `tenantid`, `process`, `adapter`, `enable`)
VALUES (5, 1, 'GETTING_DOC', 'SimulatorAdapter', 'Y');

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`,`tenantid`,`process`,`adapter`,`url`,`parameters`,`headers`,`enable`,`period`)
VALUES (6,2,'GETTING_HIST_QUOTE','TransborderAdapter','http://54.82.153.62/RestAPI.nsf/api.xsp',
'{\"limit\":\"20\",\"offset\":\"0\",\"startDate\":\"01/01/2018\"}',
'{\"Content-Type\":\"application/json\",\"Authorization\":\"Basic Sm9obiBUIENvbm5vcjpUckBuc2IwcmQzcg==\"}','Y',NULL);

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`,`tenantid`,`process`,`adapter`,`url`,`parameters`,`headers`,`enable`,`period`)
VALUES (7,2,'REQUEST_QUOTE','TransborderAdapter','http://54.82.153.62/RestAPI.nsf/api.xsp',NULL,
'{\"Content-Type\":\"application/json\",\"Authorization\":\"Basic Sm9obiBUIENvbm5vcjpUckBuc2IwcmQzcg==\"}','Y',NULL);

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`,`tenantid`,`process`,`adapter`,`url`,`parameters`,`headers`,`enable`,`period`)
VALUES (8,2,'GETTING_QUOTE','TransborderAdapter','http://54.82.153.62/RestAPI.nsf/api.xsp',
'{\"limit\":\"100\",\"offset\":\"0\", \"sleep\":\"60000\"}',
'{\"Content-Type\":\"application/json\",\"Authorization\":\"Basic Sm9obiBUIENvbm5vcjpUckBuc2IwcmQzcg==\"}','Y',60);

INSERT INTO `logixboard`.`sc_tenantintegration` (`id`, `tenantid`, `process`, `adapter`,`url`, `headers`, `enable`)
VALUES (9, 2, 'GETTING_DOC', 'TransborderAdapter', 'http://54.82.153.62/RestAPI.nsf/api.xsp',
'{\"Content-Type\":\"application/json\",\"Authorization\":\"Basic Sm9obiBUIENvbm5vcjpUckBuc2IwcmQzcg==\"}', 'Y');

--Tenant Settings
INSERT INTO `logixboard`.`sc_tenantsetting`
(`id`,`tenantid`,`type`,`value`)VALUES
(1,2,"RangesFCL20",
'[{"text":"CONT 20’ STD HASTA 8 TON EXPRESO"},{"text":"CONT 20’ STD HASTA 8 TON CONSOLIDADO"},{"text":"CONT 20’ STD HASTA 10 TON CONSOLIDADO"},
{"text":"CONT 20’ STD HASTA 12 TON CONSOLIDADO"},{"text":"CONT 20’ STD HASTA 14 TON CONSOLIDADO"},{"text":"CONT 20’ STD HASTA 17 TON CONSOLIDADO"},
{"text":"CONT 20’ STD HASTA 19 TON CONSOLIDADO"},{"text":"CONT 20’ STD HASTA 21 TON CONSOLIDADO"},{"text":"CONT 20’ STD HASTA 23 TON CONSOLIDADO"},
{"text":"CONT 20’ STD HASTA 25 TON CONSOLIDADO"}]');

INSERT INTO `logixboard`.`sc_tenantsetting`
(`id`,`tenantid`,`type`,`value`)VALUES
(2,2,"RangesFCL40",
'[{"text":"CONT 40’ STD HASTA 12 TON"},{"text":"CONT 40’ STD HASTA 15 TON"},{"text":"CONT 40’ STD HASTA 17 TON"},{"text":"CONT 40’ STD HASTA 20 TON"},
{"text":"CONT 40’ STD HASTA 22 TON"},{"text":"CONT 40’ STD HASTA 25 TON"},{"text":"CONT 40’ STD HASTA 32 TON"}]');

INSERT INTO `logixboard`.`sc_tenantsetting`
(`id`,`tenantid`,`type`,`value`)VALUES
(3,2,"RangesLCL",
'[{"text":"TURBO HASTA 4 TONELADAS"},{"text":"SENCILLO HASTA 8 TONELADAS PARA LCL"}]');

INSERT INTO `logixboard`.`sc_tenantsetting`
(`id`,`tenantid`,`type`,`value`)VALUES
(4,2,"NotificationOff",
'[{"timezone":"America/Bogota","hourini":0,"hourend":6}]');

-- DashBoard
INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'Indicator',
'SELECT count(sh.id) FROM sh_shipment sh, sh_route r, sh_routeincident  ri
 where sh.id = r.ShipmentId and r.Id = ri.RouteId and sh.status = \'Active\' and ri.Status = \'Active\' and  sh.customer = :customer',
'{"value":"","text":"label.indicator.shipments.with.incidents","htmlClass":"inline-display","icon":"fa fa-exclamation-circle","iconClass":"danger","valueFormat":"number"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'Indicator',
'SELECT COUNT(sh.id) FROM sh_shipment sh, sh_route r WHERE sh.status = \'Active\' AND r.status = \'In Transit\' AND r.shipmentid = sh.id AND sh.customer = :customer',
'{"value":"","text":"label.indicator.shipments.in.transit","htmlClass":"inline-display","icon":"","iconClass":"","valueFormat":"number"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'Indicator',
'select	count(sh.id)
from	sh_shipment sh
where	sh.status = \'Active\' and
not exists (select r.id from sh_route r where r.ShipmentId = sh.id and  r.status in (\'In Transit\', \'Completed\') )
and  sh.customer = :customer',
'{"value":"","text":"label.indicator.shipment.at.origin","htmlClass":"inline-display","icon":"","iconClass":"","valueFormat":"number"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'Indicator',
'SELECT COUNT(qt.id) FROM qt_quote qt WHERE qt.status = \'Active\' AND qt.customer = :customer',
'{"value":"","text":"label.indicator.quote.active","htmlClass":"inline-display","icon":"","iconClass":"","valueFormat":"number"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicIndicator',
'SELECT  sum(sh_charges.price) as total
FROM
sh_shipment, sh_charges, qt_quote
where
sh_shipment.quoteId = qt_quote.id and
sh_charges.ShipmentId = sh_shipment.id
and sh_shipment.status = \'Delivered\' and sh_charges.ChargeType = \'Total\'  and sh_shipment.customer =  :customer',
'{"value":"","text":"label.total.freight.spent","htmlClass":"block-display","parentHtmlClass":"col-lg-3","valueFormat":"currency"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicIndicator',
'SELECT count(sh_shipment.id) FROM sh_shipment, qt_quote
where sh_shipment.QuoteId = qt_quote.id and sh_shipment.status = \'Delivered\' and  sh_shipment.customer = :customer',
'{"value":"","text":"label.shipments.delivered","htmlClass":"block-display","parentHtmlClass":"col-lg-3","valueFormat":"number"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicIndicator',
'SELECT
sum(
	case
		when sh_container.Size = \'20 FT\' then 1
		when sh_container.Size = \'40 FT\' then 2
        when sh_container.Size = \'40 FT High Cube\' then 2
        when sh_container.Size = \'45 FT High Cube\' then 2
    else 0 END) as total
FROM
sh_shipment, qt_quote, sh_container
	where
		sh_shipment.QuoteId = qt_quote.id and
        sh_container.Type = \'FCL\' and
        sh_container.ShipmentId = sh_shipment.id and
        sh_shipment.status = \'Delivered\' and
        sh_shipment.customer = :customer',
'{"value":"","text":"TEUs","htmlClass":"block-display","parentHtmlClass":"col-lg-2","valueFormat":"number"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicIndicator',
'SELECT sum((case when qt_quotedetail.WeightUnits = \'kg/cbm\' then (sh_dimension.length * sh_dimension.height * sh_dimension.width) else ( ((sh_dimension.length/0.0328084)*100)/100 * ((sh_dimension.height/0.0328084)*100)/100 * ((sh_dimension.width/0.0328084)*100)/100) END) * sh_dimension.piececount / 1000000 )  as total
FROM
sh_shipment, qt_quote, qt_quotedetail, sh_dimension
where
sh_shipment.QuoteId = qt_quote.id
and qt_quote.QuoteDetailId = qt_quotedetail.id
and sh_dimension.ShipmentId = sh_shipment.id
and sh_shipment.status = \'Delivered\' and  sh_shipment.customer = :customer',
'{"value":"","text":"CBMs","htmlClass":"block-display","parentHtmlClass":"col-lg-2","valueFormat":"number"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicIndicator',
'SELECT sum((case when qt_quotedetail.WeightUnits = \'kg/cbm\' then sh_dimension.Weight else sh_dimension.Weight / 2.2 END) * sh_dimension.PieceCount) as total FROM  sh_shipment, qt_quote, qt_quotedetail, sh_dimension where  sh_shipment.QuoteId = qt_quote.id and qt_quote.QuoteDetailId = qt_quotedetail.id and sh_dimension.ShipmentId = sh_shipment.id and sh_shipment.status = \'Delivered\' and  sh_shipment.customer = :customer',
'{"value":"","text":"KGs","htmlClass":"block-display","parentHtmlClass":"col-lg-2","valueFormat":"number"}');

/*INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicIndicator',
'SELECT
	sum(qt_quotedetail.TotalShipmentValue)
FROM qt_quotedetail, qt_quote, sh_shipment
	where
		sh_shipment.QuoteId = qt_quote.id and
        qt_quote.QuoteDetailId = qt_quotedetail.id and
        sh_shipment.status = \'Delivered\'  and
        qt_quote.customer = :customer',
'{"value":"","text":"label.commodity.value","htmlClass":"block-display","valueFormat":"currency"}');*/

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'HistoricalIndicator',
'SELECT count(qt_quote.id) FROM qt_quote where month(qt_quote.CreationDate) = month(curdate()) and qt_quote.Status = ''Accepted'' and qt_quote.Customer = :customer
union all
SELECT count(qt_quote.id) FROM qt_quote where month(qt_quote.CreationDate) = month(curdate()) and qt_quote.Customer = :customer',
'{"label1":"Quotes Accepted","value1":"","label2":"Quotes Requested","value2":"","color1":"#20b7a1","graphTitle":"getCurrentMonth()","graphType":"getCurrentMonth()","graphFooter":"label.quotes.accepted.requested"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'HistoricalIndicator',
'SELECT count(qt_quote.id) FROM qt_quote where quarter(qt_quote.CreationDate) = quarter(curdate()) and qt_quote.Status = ''Accepted'' and qt_quote.Customer = :customer
union all
SELECT count(qt_quote.id) FROM qt_quote where quarter(qt_quote.CreationDate) = quarter(curdate()) and qt_quote.Customer = :customer',
'{"label1":"Quotes Accepted","value1":"","label2":"Quotes Requested","value2":"","color1":"#20b7a1","graphTitle":"getCurrentQuarter()","graphType":"getCurrentQuarter()","graphFooter":"label.quotes.accepted.requested"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'HistoricalIndicator',
'SELECT count(qt_quote.id) FROM qt_quote where year(qt_quote.CreationDate) = year(curdate()) and qt_quote.Status = ''Accepted'' and qt_quote.Customer = :customer
union all
SELECT count(qt_quote.id) FROM qt_quote where year(qt_quote.CreationDate) = year(curdate()) and qt_quote.Customer = :customer',
'{"label1":"Quotes Accepted","value1":"","label2":"Quotes Requested","value2":"","color1":"#20b7a1","graphTitle":"getCurrentYear()","graphType":"getCurrentYear()","graphFooter":"label.quotes.accepted.requested"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'HistoricalIndicator',
'SELECT COUNT(delivered.Id)
FROM
	(SELECT  sh_shipment.Id
	FROM sh_shipment
	INNER JOIN
		(SELECT DISTINCT sh_route.ShipmentId
		FROM sh_route
		INNER JOIN sh_routeincident ON sh_route.Id=sh_routeincident.RouteId) AS routes
	ON sh_shipment.Id=routes.ShipmentId AND sh_shipment.Status=''Delivered'' AND sh_shipment.Customer = :customer AND month(sh_shipment.CreationDate) = month(curdate())) AS delivered
UNION ALL
SELECT COUNT( sh_shipment.Id) FROM sh_shipment WHERE month(sh_shipment.CreationDate) = month(curdate()) AND sh_shipment.Customer = :customer AND Status=''Delivered''',
'{"label1":"Shipments delivered with incidences","value1":"","label2":"Shipments Delivered","value2":"","color1":"#ED3F54","graphTitle":"getCurrentMonth()","graphType":"getCurrentMonth()","graphFooter":"label.shipments.incidences.delivered"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'HistoricalIndicator',
'SELECT COUNT(delivered.Id)
FROM
	(SELECT  sh_shipment.Id
	FROM sh_shipment
	INNER JOIN
		(SELECT DISTINCT sh_route.ShipmentId
		FROM sh_route
		INNER JOIN sh_routeincident ON sh_route.Id=sh_routeincident.RouteId) AS routes
	ON sh_shipment.Id=routes.ShipmentId AND sh_shipment.Status=''Delivered'' AND sh_shipment.Customer = :customer AND quarter(sh_shipment.CreationDate) = quarter(curdate())) AS delivered
UNION ALL
SELECT COUNT( sh_shipment.Id) FROM sh_shipment WHERE quarter(sh_shipment.CreationDate) = quarter(curdate()) AND sh_shipment.Customer = :customer AND Status=''Delivered''',
'{"label1":"Shipments delivered with incidences","value1":"","label2":"Shipments Delivered","value2":"","color1":"#ED3F54","graphTitle":"getCurrentQuarter()","graphType":"getCurrentQuarter()","graphFooter":"label.shipments.incidences.delivered"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'HistoricalIndicator',
'SELECT COUNT(delivered.Id)
FROM
	(SELECT  sh_shipment.Id
	FROM sh_shipment
	INNER JOIN
		(SELECT DISTINCT sh_route.ShipmentId
		FROM sh_route
		INNER JOIN sh_routeincident ON sh_route.Id=sh_routeincident.RouteId) AS routes
	ON sh_shipment.Id=routes.ShipmentId AND sh_shipment.Status=''Delivered'' AND sh_shipment.Customer = :customer AND year(sh_shipment.CreationDate) = year(curdate())) AS delivered
UNION ALL
SELECT COUNT( sh_shipment.Id) FROM sh_shipment WHERE year(sh_shipment.CreationDate) = year(curdate()) AND sh_shipment.Customer = :customer AND Status=''Delivered''',
'{"label1":"Shipments delivered with incidences","value1":"","label2":"Shipments Delivered","value2":"","color1":"#ED3F54","graphTitle":"getCurrentYear()","graphType":"getCurrentYear()","graphFooter":"label.shipments.incidences.delivered"}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicGraphIndicator',
'SELECT
    DATE_FORMAT(sh_shipment.LastUpdate, ''%Y-%m-01''),
    SUM(sh_charges.price) AS total
FROM
    sh_shipment,
    sh_charges,
    qt_quote
where
    sh_shipment.quoteId = qt_quote.id and
    sh_charges.shipmentId = sh_shipment.id
        AND sh_shipment.status = ''Delivered''
        AND sh_charges.ChargeType = ''Total''
        AND sh_shipment.customer = :customer',
'{"name":"totalFreightSpend","title":"label.total.freight.spent","specifier":"$,", "chartData": [], "xChart": []}');

/*INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicGraphIndicator',
'SELECT DATE_FORMAT(sh_shipment.CreationDate, "%Y-%m-01"), sum(qt_quotedetail.TotalShipmentValue)
 FROM sh_shipment, qt_quote, qt_quotedetail
 where sh_shipment.QuoteId = qt_quote.id
 and qt_quote.QuoteDetailId = qt_quotedetail.id
 and sh_shipment.status = ''Delivered''
 and  sh_shipment.customer = :customer',
'{"name":"commodityValue","title":"label.commodity.value","specifier":"$,", "chartData": [], "xChart": []}');*/

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicGraphIndicator',
'SELECT
	DATE_FORMAT(sh_shipment.LastUpdate, "%Y-%m-01"),
	sum(sh_charges.price)/count(sh_shipment.id)  as total
    FROM sh_shipment,sh_charges,
    qt_quote
where
  sh_shipment.quoteId = qt_quote.id and
  sh_charges.ShipmentId = sh_shipment.id and
				sh_shipment.status = ''Delivered''  AND sh_charges.ChargeType = ''Total'' AND
        sh_shipment.customer = :customer',
'{"name":"averageSpendShipment","title":"label.average.spend.shipment","specifier":"$,", "chartData": [], "xChart": []}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicGraphIndicator',
'SELECT DATE_FORMAT(sh_shipment.LastUpdate, "%Y-%m-01"), count(sh_shipment.id)
 FROM sh_shipment, qt_quote
 where sh_shipment.QuoteId = qt_quote.id
 and sh_shipment.status = ''Delivered''
 and  sh_shipment.customer = :customer',
'{"name":"shipmentsDelivered","title":"label.shipments.delivered","specifier":"", "chartData": [], "xChart": []}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicGraphIndicator',
'SELECT DATE_FORMAT(sh_shipment.LastUpdate, "%Y-%m-01"),
 sum(case
      when sh_container.Size = ''20 FT'' then 1
      when sh_container.Size = ''40 FT'' then 2
      when sh_container.Size = ''40 FT High Cube'' then 2
      when sh_container.Size = ''45 FT High Cube'' then 2
      else 0 END) as total
 FROM sh_shipment, qt_quote, sh_container
 where sh_shipment.QuoteId = qt_quote.id
 and sh_container.Type = ''FCL''
 and sh_container.ShipmentId = sh_shipment.id
 and sh_shipment.status = ''Delivered''
 and sh_shipment.customer = :customer',
'{"name":"teuDelivered","title":"label.teu.delivered","specifier":"", "chartData": [], "xChart": []}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicGraphIndicator',
'SELECT DATE_FORMAT(sh_shipment.LastUpdate, "%Y-%m-01"),
 sum(
  (case when qt_quotedetail.WeightUnits = ''kg/cbm'' then (sh_dimension.length * sh_dimension.height * sh_dimension.width)
   else (((sh_dimension.length/0.0328084)*100)/100 * ((sh_dimension.height/0.0328084)*100)/100 * ((sh_dimension.width/0.0328084)*100)/100) END) * sh_dimension.piececount / 1000000 )
 FROM sh_shipment, qt_quote, qt_quotedetail, sh_dimension
 where sh_shipment.QuoteId = qt_quote.id
 and qt_quote.QuoteDetailId = qt_quotedetail.id
 and sh_dimension.ShipmentId = sh_shipment.id
 and sh_shipment.status = ''Delivered''
 and  sh_shipment.customer = :customer',
'{"name":"cbmDelivered","title":"label.cbm.delivered","specifier":"", "chartData": [], "xChart": []}');

INSERT INTO `logixboard`.`sc_customerdashboard` (`customerid`, `type`, `sqlquery`, `response`)
VALUES ('0', 'DynamicGraphIndicator',
'SELECT DATE_FORMAT(sh_shipment.LastUpdate, "%Y-%m-01"),
sum(((case when qt_quotedetail.WeightUnits = ''kg/cbm'' then sh_dimension.Weight else sh_dimension.Weight / 2.2 END) * sh_dimension.PieceCount))
FROM sh_shipment, qt_quote, qt_quotedetail, sh_dimension
	where sh_shipment.QuoteId = qt_quote.id
		and qt_quote.QuoteDetailId = qt_quotedetail.id
		and sh_dimension.ShipmentId = sh_shipment.id
		and sh_shipment.status = ''Delivered''
		and  sh_shipment.customer = :customer',
'{"name":"chargeableKgsDelivered","title":"label.chargeable.kgs.delivered","specifier":"", "chartData": [], "xChart": []}');